version=0.4

.PSEUDO: dist
dist: dist/busradar-${version}-linux-amd64.tar.gz dist/busradar-${version}-windows-amd64.zip dist/busradar-${version}-windows-386.zip

dist/busradar-${version}-linux-amd64.tar.gz: busradar
	[ -d dist ] || mkdir dist
	tar czf dist/busradar-${version}-linux-amd64.tar.gz busradar contrib README.md

dist/busradar-${version}-windows-amd64.zip: busradar-amd64.exe
	[ -d dist ] || mkdir dist
	zip -r dist/busrdar-${version}-windows-amd64.zip busradar-amd64.exe contrib README.md

dist/busradar-${version}-windows-386.zip: busradar-386.exe
	[ -d dist ] || mkdir dist
	zip -r dist/busrdar-${version}-windows-386.zip busradar-386.exe contrib README.md

busradar: *.go traffic/*.go
	go build

busradar-amd64.exe: *.go traffic/*.go
	PATH=/home/devel/go/bin:$$PATH GOROOT=/home/devel/go GOOS=windows GOARCH=amd64 go build -o busradar-amd64.exe

busradar-386.exe: *.go traffic/*.go
	PATH=/home/devel/go/bin:$$PATH GOROOT=/home/devel/go GOOS=windows GOARCH=386 go build -o busradar-386.exe

.PSEUDO: clean purge
clean:
	-rm busradar busradar-amd64.exe busradar-386.exe

purge: clean
	-rm -r dist

