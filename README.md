BusRadar Backend
================

This repository hosts the backend to the BusRadar app. At the moment, all it
does is start a simulation of a few buses.

At the moment it starts a server on localhost:8080. The following endpoints are
implemented:

    /
    /buses
    /stops
    /lines
    /sstops

## Installation
You need to install the [Go](http://golang.org) compiler tools to compile this,
or just download the binaries from the download page.

## Running
The program options look like:

    ./busradar [-quiet] [<directory-to-config-files>]

If you want to run the executable just by clicking on it, drop it in the
`contrib` folder and then run it, otherwise run it like this:

    ./busradar contrib

If you are on Windows, then something like this:

    busradar-amd64.exe contrib

So not much difference. :-)

## News
#### Version 0.4 (27. June 2014)
This release fixes a few bugs and adds an important feature for the android
device: simplified stops, which gives us a list of stops that are an average
of the directioned stops with the same name.

 - New: option `-quiet` supresses the messages of where the bus is.
 - New: endpoint `/sstops` implemented which averages stop coordinates.
 - Fix: race conditions have been solved at no real performance expense;
   this solves the known issue in version 0.3.
 - Fix issue #1: `NextETA` gave excessively high answers due to incorrect
   implementation of `secondsToNextStop()` function.

Miscellaneous code changes:

 - New: `traffic` package has a little test framework now.
 - New: `ReadDir` function created to simplify code a little.

#### Version 0.3 (25. June 2014)
This release brings the configuration into the files and out of the
application. The big change here is twofold: you can specify on the command
line what directory to look in for configuration files, and the configuration
files now contain everything that is needed for simulation. These files are
loaded (though only at the start).

 - New: dynamic configuration through files.
 - New: README file in contrib.
 - New: Buses for the line 214 are in contrib.
 - New: When the bus reaches a stop, the `NextETA` attribute is 0, and the
   `NextStop` attribute indicates which stop the bus is at.
 - Change: looks per default in current directory, specify directory via
   command line argument (e.g. `./busradar contrib`)
 - Bugfix: previously, the config files had latitude and longitude switched.

There are some known issues:

 - Reading from `/buses` may give inaccurate results, as the locking has been
   lifted for performance reasons.

#### Version 0.2 (21. June 2014)
This is the first update to the backend, which is mainly about streamlining
things. The next few will probably also contain a lot of bugfixes,
restructuring and so on.

 - New: All buses are started at the same time which means that you have
     a complete system right away.
 - Update: Better documentation and error reporting.

#### Version 0.1 (20. June 2014)
This is the initial release.

 - A server on localhost:8080 that gives four endpoints, three in JSON.
 - Many bus stops, one line, one route, five buses in simulation.
 - Dynamic updating of buses.
 - Package traffic available.

## Contribution
Contributions are welcome. If you have any issues or any requests, please make
them!
