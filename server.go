// Copyright (c) 2014, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/cassava/busradar/traffic"

	"github.com/gorilla/mux"
)

func StartServer(t *traffic.Traffic) {
	// Setup the multiplexer
	r := mux.NewRouter()
	r.HandleFunc("/", homeHandler)
	r.HandleFunc("/buses", trafficHandler(t.Buses))
	r.HandleFunc("/lines", trafficHandler(t.Lines))
	r.HandleFunc("/stops", trafficHandler(t.Stops))
	r.HandleFunc("/sstops", trafficHandler(traffic.SimplifyStops(t.Stops)))

	// Start the server
	log.Println("Starting server on localhost:7070...")
	http.Handle("/", r)
	log.Fatal(http.ListenAndServe(":7070", nil))
}

func trafficHandler(v interface{}) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		b, err := json.Marshal(v)
		if err != nil {
			internalError(w, r, err)
			return
		}

		buf := bytes.NewBuffer(b)
		buf.WriteTo(w)
	}
}

func homeHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w,
		`Welcome to the BusRadar server.

You may query this server for traffic information,
which will then return a JSON response.
The following endpoints are available:

	/
	/buses
		?pos=<pos>
		?dim=<pos>x<pos>
		/<id>
	/stops
		?pos<pos>
		?dim=<pos>x<pos>
		/<name>
	/lines
		?pos<pos>
		?dim=<pos>x<pos>
		/<name>
	/sstops

Of these endpoints currently only the most basic are
actually implemented at the moment.
`)
}

func internalError(w http.ResponseWriter, _ *http.Request, v ...interface{}) {
	b := make([]interface{}, 0, len(v)+1)
	b = append(b, "500 Internal Server Error:\n")
	b = append(b, v...)
	fmt.Fprintln(w, b...)
}
