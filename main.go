// Copyright (c) 2014, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"bitbucket.org/cassava/busradar/traffic"
)

func main() {
	var dir string
	var quiet bool

	log.Println("busradar version 0.4 (27. June 2014)")
	flag.BoolVar(&quiet, "quiet", false, "don't print when each bus arrives")
	flag.Parse()
	if flag.NArg() > 0 {
		dir = flag.Arg(0)
	}

	ch := make(chan error)
	go func() {
		for err := range ch {
			assert(err)
		}
	}()

	t, err := traffic.ReadDir(dir, ch)
	assert(err)

	for _, b := range t.Buses {
		if !quiet {
			b.RegisterStopHook(func(b *traffic.Bus, r *traffic.Route, idx int) {
				log.Printf("bus %d: %s", b.ID, r.Line.Stops[idx].Name)
			})
		}
		b.Start()
	}

	StartServer(t)
}

func assert(err error) {
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
