README to Configuration Files
=============================

When the BusRadar backend starts, it has to load some configuration files so
that it knows where the stops, lines, and bus routes are. It loads files based
on a certain file format, and in each file there are comments to specify extra
information. Comments have the following format:

    # option value...

The first word following a `#` character and any spaces is the option, and all
the rest of the line is the value of that option.

If you would like to help, you are free to do so. I recommend you use Google
Earth to create the routes and the stops.

## Stops
A file describing the stops is a file `stops*` containing three comma separated
values on each line:

    longitude,latitude,name direction

The name and direction are assumed to be unique across ALL stops files. Any
stops loaded later with the same name will overwrite the previous. There are no
options.

Here is an example, taken from `stops_10.csv`:

    9.931876091646217,49.78604797706162,Sanderglacisstraße SW
    9.934643149987016,49.78599361103635,Studentenhaus SW
    9.937077732441427,49.78433419682982,Adalberokirche W
    9.941771284102641,49.78344272988843,Fichtestraße NW
    9.945254420790851,49.78382746711856,Erthalstraße NW
    ...

## Lines
A file describing a line is a file `line*` containing one value on each line:

    stop-id

There are options that need to be set for a line:

    # line <uid>
    # name <string>

The option `line` needs to give a unique ID; as this will be referenced by
routes.  The uid should not contain any newlines or commas. If the name is
omitted, then the line ID doubles as the name.

Here is an example, taken from `line_214.csv`:

    # line 214
    Busbahnhof E
    Barbarossaplatz E
    Mainfranken Theater S
    Rennweg SE

## Routes
A file describing a route that a bus can take is a file `route*` containing two
comma-separated values on each line:

    latitude,longitude

There are options that need to be set for a route:

    # route <uid>
    # line <uid>

The option `route` gives the route a unique id which can be referenced by
buses. The `line` option states which line the route is associated with.

Here is an example, taken from `route_10_0.csv`:

    # route 10.0
    # line 10
    9.93187693597471,49.78615914347613
    9.931889456673426,49.78612950447287
    9.931905219998621,49.78609471147843
    9.931912663951321,49.78607025603242
    9.931916648585009,49.7860524398814
    9.931914870300705,49.78602530556581
    9.931915966211506,49.78600297766797

## Buses
A file describing buses that drive along a route is a file `buses*` containing
five comma-separated values.

    uid,route,roundtrip,stoptime,percentile

The value `uid` is the unique number of the bus. The value `route` is the ID of
the route that the bus will travel. The value `roundtrip` is the time  that the
bus needs to make the entire roundtrip. The value `stoptime` is the time that
the bus stops after reaching a stop; this is counted to roundtrip.

Here is an example, taken from `buses_10.csv`:

    1001,10.0,25m,10s,0.0
    1002,10.0,25m,15s,0.2
    1003,10.0,24m,10s,0.4
    1004,10.0,26m,20s,0.6
    1005,10.0,23m,5s,0.8
