// Copyright (c) 2014, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package traffic

import (
	"bufio"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

type Bus struct {
	ID int // Unique ID of the Bus

	route *Route        // route is the Route that the bus travels along
	wait  time.Duration // wait is the time to wait till updating position

	pos   int        // pos is the index to the current position on the route
	next  int        // next is the index to the next stop
	mutex sync.Mutex // mutex guards pos and next and needs to be locked before changing them

	state  BusState      // state specifies the current BusState of the bus and is only changed by change
	change chan BusState // change is for signalling the runner to stop or reset

	move_hooks []CallbackFunc // hooks called when position is updated
	stop_hooks []CallbackFunc // hooks that are called when stop is reached
}

type CallbackFunc func(*Bus, *Route, int)

type BusState int

const (
	BusReset BusState = iota
	BusRunning
	BusStopped
)

var busID int

func NewBus(r *Route) *Bus {
	busID++
	return &Bus{
		ID:     busID,
		route:  r,
		change: make(chan BusState),
	}
}

// Clone returns a copy of the Bus b, including the route and the hooks.
// The current position and state of the bus are reset however.
func (b *Bus) Clone() *Bus {
	busID++
	c := &Bus{
		ID:         busID,
		route:      b.route,
		wait:       b.wait,
		pos:        0,
		next:       0,
		state:      BusReset,
		change:     make(chan BusState),
		move_hooks: make([]CallbackFunc, len(b.move_hooks)),
		stop_hooks: make([]CallbackFunc, len(b.stop_hooks)),
	}
	copy(c.move_hooks, b.move_hooks)
	copy(c.stop_hooks, b.stop_hooks)
	return c
}

// jsonBus is used when creating a JSON representation of the Bus.
type jsonBus struct {
	ID       int
	Line     string
	Coord    Coord
	NextStop *Stop
	NextETA  uint64
}

// MarshalJSON returns a JSON representation of the Bus. We have an extra
// method because there is extra state that we want to calculate, as well as
// much that we don't need.
func (b *Bus) MarshalJSON() ([]byte, error) {
	b.mutex.Lock()
	defer b.mutex.Unlock()
	return json.Marshal(&jsonBus{
		ID:       b.ID,
		Line:     b.route.Line.Name,
		Coord:    b.Coord(),
		NextStop: b.route.Line.Stops[b.next],
		NextETA:  b.secondsToStop(b.next),
	})
}

// Coord returns the current position of the bus.
func (b *Bus) Coord() Coord {
	return b.route.Path[b.pos]
}

// BusState returns the current state of the bus.
func (b *Bus) State() BusState {
	return b.state
}

// Speed returns the number of markers per second that are passed.
func (b *Bus) Speed() float64 {
	if b.wait > 0.0 {
		return float64(time.Second) / float64(b.wait)
	}
	return -1.0
}

// SetSpeed sets the number of markers passed per second.
func (b *Bus) SetSpeed(mps float64) {
	b.wait = 0.0
	if mps > 0.0 {
		b.wait = time.Duration(float64(time.Second) / mps)
	}
}

func (b *Bus) SetRoundtrip(roundtrip time.Duration) {
	b.wait = b.route.WaitTime(roundtrip)
}

func (b *Bus) SetPosition(percentile float64) {
	b.pos = int(float64(len(b.route.Path)) * percentile)
	for i, s := range b.route.StopIndexes {
		if s > b.pos {
			b.next = i
			break
		}
	}
}

// RegisterMoveHook registers a function to be called whenever the bus updates
// its position.
func (b *Bus) RegisterMoveHook(f CallbackFunc) {
	b.move_hooks = append(b.move_hooks, f)
}

// RegisterStopHook registers a function to be called whenever the bus reaches
// a new bus stop.
func (b *Bus) RegisterStopHook(f CallbackFunc) {
	b.stop_hooks = append(b.stop_hooks, f)
}

// valid returns an error if the bus is not in a valid state (that is, good
// for running) otherwise nil.
func (b *Bus) valid() error {
	if err := b.route.valid(); err != nil {
		return fmt.Errorf("bus %d: %s", b.ID, err)
	} else if b.next >= len(b.route.StopIndexes) || b.next < 0 {
		return fmt.Errorf("bus %d: next is out of bounds", b.ID)
	} else if b.pos >= len(b.route.Path) || b.pos < 0 {
		return fmt.Errorf("bus %d: pos is out of bounds", b.ID)
	}
	return nil
}

// Start starts moving the Bus along the set route, if it is not empty.
func (b *Bus) Start() {
	switch b.state {
	case BusReset:
		go b.run()
	case BusStopped:
		b.change <- BusRunning
	default:
		return
	}
}

// StartIn starts a bus not at the beginning of the route, but in the percentile defined.
// The parameter percentile should be between 0 and 1.0, otherwise there will probably
// be a bound overflow.
//
// Example:
//
// 	for i := 1; i < n; i++ {
// 		b := bus.Clone()
// 		b.StartIn(float64(i) / float64(n))
// 	}
//
func (b *Bus) StartIn(percentile float64) {
	if err := b.valid(); err != nil {
		log.Println(err)
		return
	}

	b.SetPosition(percentile)
	b.Start()
}

// StartAfter sleeps for the given duration and then starts the bus.
// It is generally recommended to use the StartIn function however.
func (b *Bus) StartAfter(dur time.Duration) {
	time.Sleep(dur)
	b.Start()
}

// run is a function that is run in it's own goroutine, and it updates the
// position and the current and next bus stops, and calls the functions that
// are registered.
func (b *Bus) run() {
	r := b.route
	n := len(r.Path)
	alive := true
	move := make(chan int)

	travel := func() {
		time.AfterFunc(b.wait, func() {
			if alive {
				move <- (b.pos + 1) % n
			}
		})
	}

	b.state = BusRunning
	go func() { move <- b.pos }()
	for {
		select {
		case b.state = <-b.change:
			if b.state == BusStopped {
				// If it's stopped, it can only be BusRunning or BusReset.
				// The latter case we handle below by simply falling through.
				b.state = <-b.change
				if b.state == BusRunning {
					continue
				}
			}
			if b.state == BusReset {
				alive = false
				b.reset()
				return
			}
		case npos := <-move:
			triggerNext := -1

			// Update position and next index
			b.mutex.Lock()
			b.pos = npos
			if b.pos == r.StopIndexes[b.next] {
				triggerNext = b.next
				b.next = (b.next + 1) % len(r.Line.Stops)
			}
			b.mutex.Unlock()

			// Run registered callbacks
			for _, f := range b.move_hooks {
				f(b, r, b.pos)
			}
			if triggerNext != -1 {
				for _, f := range b.stop_hooks {
					f(b, r, b.next)
				}
			}

			// Start next move
			go travel()
		}
	}
}

// Stop stops (pauses) a running bus.
func (b *Bus) Stop() {
	if b.state == BusRunning {
		b.change <- BusStopped
	}
}

// Reset resets a running or stopped bus, but note that it does not change
// the speed or the route or the hooks.
func (b *Bus) Reset() {
	if b.state != BusReset {
		b.change <- BusReset
	}
}

// reset does the actual resetting, we can assume that the state is zero.
func (b *Bus) reset() {
	b.pos = 0
	b.next = 0
	b.state = BusReset
}

// secondsToStop returns the remaining time to the next stop, assuming
// that the bus is running.
func (b *Bus) secondsToStop(idx int) uint64 {
	np := b.route.StopIndexes[idx]
	var diff int
	if np < b.pos {
		diff = (len(b.route.Path) - b.pos) + np
	} else {
		diff = np - b.pos
	}
	s := float64(diff) / b.Speed()
	return uint64(s)
}

func WaitFunc(dur time.Duration) CallbackFunc {
	return func(_ *Bus, _ *Route, _ int) {
		time.Sleep(dur)
	}
}

func WaitSecondsFunc(secs int) CallbackFunc {
	return WaitFunc(time.Duration(secs) * time.Second)
}

func ReadBuses(filename string, m map[string]*Route) ([]*Bus, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	buf := bufio.NewReader(file)

	var buses []*Bus
	err = foreachLine(buf, func(s string) error {
		vals := strings.Split(s, ",")
		if len(vals) != 5 {
			return fmt.Errorf("not enough values in %q", s)
		}

		var b Bus
		b.change = make(chan BusState)

		// 0: ID
		var err error
		b.ID, err = strconv.Atoi(vals[0])
		if err != nil {
			return fmt.Errorf("cannot parse ID (0) from %q: %s", s, err)
		}

		// 1: Route ID
		var ok bool
		if b.route, ok = m[vals[1]]; !ok {
			return fmt.Errorf("unknown route %s", vals[1])
		}

		// 3: Wait
		wait, err := time.ParseDuration(vals[3])
		if err != nil {
			return fmt.Errorf("cannot parse wait (3) from %q: %s", s, err)
		}
		b.RegisterStopHook(WaitFunc(wait))

		// 2: Roundtrip
		dur, err := time.ParseDuration(vals[2])
		if err != nil {
			return fmt.Errorf("cannot parse roundtrip (2) from %q: %s", s, err)
		}
		b.SetRoundtrip(dur - wait*time.Duration(len(b.route.Line.Stops)))

		// 4: Percentile
		percent, err := strconv.ParseFloat(vals[4], 64)
		if err != nil {
			return fmt.Errorf("cannot parse percentile (4) from %q: %s", s, err)
		}
		b.SetPosition(percent)

		buses = append(buses, &b)
		return nil
	})
	if err != nil {
		return nil, NewParseError(filename, err)
	}

	return buses, nil
}

func ReadAllBuses(filenames []string, m map[string]*Route, ch chan<- error) []*Bus {
	var n int
	list := make([][]*Bus, 0, len(filenames))
	for _, f := range filenames {
		bs, err := ReadBuses(f, m)
		if err != nil {
			if ch != nil {
				ch <- err
			}
			continue
		}

		list = append(list, bs)
		n += len(bs)
	}

	i := 0
	buses := make([]*Bus, n)
	for _, bs := range list {
		for _, b := range bs {
			buses[i] = b
			i++
		}
	}
	return buses
}
