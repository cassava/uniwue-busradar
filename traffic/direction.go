// Copyright (c) 2014, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package traffic

type Direction int

const (
	UnknownDirection Direction = iota
	N
	NW
	W
	SW
	S
	SE
	E
	NE
)

func ParseDirection(s string) Direction {
	switch s {
	case "N":
		return N
	case "NW":
		return NW
	case "W":
		return W
	case "SW":
		return SW
	case "S":
		return S
	case "SE":
		return SE
	case "E":
		return E
	case "NE":
		return NE
	default:
		return UnknownDirection
	}
}
