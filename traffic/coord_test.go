// Copyright (c) 2014, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package traffic

import (
	"bufio"
	"os"
	"strings"
)

func readCoords(filename string) ([]Coord, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	// Read the coordinates from the file.
	var coords []Coord
	buf := bufio.NewReader(file)
	err = foreachLine(buf, func(s string) error {
		if strings.HasPrefix(s, "#") {
			return nil
		}

		c, err := ParseCoord(s)
		if err != nil {
			return err
		}
		coords = append(coords, c)
		return nil
	})
	if err != nil {
		return nil, err
	}

	return coords, nil
}
