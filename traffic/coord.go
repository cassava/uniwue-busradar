// Copyright (c) 2014, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package traffic

import (
	"fmt"
	"math"
	"strconv"
	"strings"
)

// Coord represents a geographical coordinate via latitude and longitude.
type Coord struct {
	Lat  float64
	Long float64
}

func (c Coord) String() string {
	return fmt.Sprintf("%f,%f", c.Lat, c.Long)
}

func (c Coord) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf(`"%f,%f"`, c.Lat, c.Long)), nil
}

// Dist returns the distance between the two coordinates a and b.
func (c Coord) Dist(b Coord) float64 {
	d1 := math.Abs(b.Lat - c.Lat)
	d2 := math.Abs(b.Long - c.Long)
	return math.Sqrt(d1*d1 + d2*d2)
}

// Midpoint returns the midpoint between c and b.
func (c Coord) Midpoint(b Coord) Coord {
	return Coord{
		Lat:  (c.Lat + b.Lat) / 2,
		Long: (c.Long + b.Long) / 2,
	}
}

// NewCoord returns a new Coord.
func NewCoord(lat, long float64) Coord {
	return Coord{Lat: lat, Long: long}
}

// ParseCoord parses a Coord from a string.
func ParseCoord(s string) (c Coord, err error) {
	arr := strings.Split(s, ",")
	if len(arr) != 2 {
		return c, coordParseErr(s)
	}

	c, err = parseCoordParts(arr[0], arr[1])
	if err != nil {
		return c, coordParseErr(s)
	}
	return c, nil
}

func parseCoordParts(lat, long string) (c Coord, err error) {
	c.Lat, err = strconv.ParseFloat(lat, 64)
	if err != nil {
		return c, err
	}
	c.Long, err = strconv.ParseFloat(long, 64)
	if err != nil {
		return c, err
	}
	return c, nil
}

func coordParseErr(s string) error {
	return fmt.Errorf("cannot parse Coord %q", s)
}
