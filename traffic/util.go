// Copyright (c) 2014, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package traffic

import (
	"bufio"
	"io"
	"strings"
)

// foreachLine executes f for each non-empty line in the reader.
func foreachLine(buf *bufio.Reader, f func(string) error) error {
	for {
		line, err := buf.ReadString('\n')
		if err != nil && len(line) == 0 {
			if err == io.EOF {
				break
			}
			return err
		}
		line = strings.TrimSpace(line)
		if len(line) == 0 {
			continue
		}

		if err = f(line); err != nil {
			return err
		}
	}
	return nil
}

// getOpt returns m[k] if ok otherwise def.
func getOpt(m map[string]string, k, def string) string {
	v := m[k]
	if len(v) == 0 {
		return def
	}
	return v
}

// readOptions reads all the comments and empty lines in the beginning of the
// file until it hits a line that is not a comment. Options that it reads
// from the comments are stored in the map. It assumes that everything is an
// option actually.
func readOptions(buf *bufio.Reader) (map[string]string, error) {
	opt := make(map[string]string)
	for {
		// Try to find out if the next line is commented or not, before reading
		// it and it not being one (which would mean lost information).
		bs, err := buf.Peek(32)
		if err != nil && len(bs) == 0 {
			if err == io.EOF {
				break
			} else if err != bufio.ErrBufferFull {
				return nil, err
			}
		}

		s := strings.TrimSpace(string(bs))
		if !strings.HasPrefix(s, "#") {
			break
		}

		// Read the line, since it is a comment, and store the option.
		line, err := buf.ReadString('\n')
		if err != nil && len(line) != 0 {
			if err == io.EOF {
				break
			}
			return nil, err
		}

		line = strings.Trim(line, "# \n\t\r")
		if len(line) == 0 {
			continue
		}

		kv := strings.SplitN(line, " ", 2)
		if len(kv) == 2 {
			opt[kv[0]] = kv[1]
		} else if len(kv) == 1 {
			opt[kv[0]] = ""
		}
	}

	return opt, nil
}
