// Copyright (c) 2014, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package traffic

import (
	"path"
	"path/filepath"
)

type Traffic struct {
	Stops []*Stop
	Lines []*Line
	Buses []*Bus
}

func ReadDir(dir string, ch chan<- error) (*Traffic, error) {
	var t Traffic

	// Stops
	glob, err := filepath.Glob(path.Join(dir, "stops*"))
	if err != nil {
		return nil, err
	}
	t.Stops = ReadAllStops(glob, ch)
	stopMap := StopsAsMap(t.Stops, ch)

	// Lines
	glob, err = filepath.Glob(path.Join(dir, "line*"))
	if err != nil {
		return nil, err
	}
	t.Lines = ReadAllLines(glob, stopMap, ch)
	lineMap := LinesAsMap(t.Lines, ch)

	// Routes
	glob, err = filepath.Glob(path.Join(dir, "route*"))
	if err != nil {
		return nil, err
	}
	routeMap := RoutesAsMap(ReadAllRoutes(glob, lineMap, ch), ch)

	// Buses
	glob, err = filepath.Glob(path.Join(dir, "buses*"))
	if err != nil {
		return nil, err
	}
	t.Buses = ReadAllBuses(glob, routeMap, ch)

	return &t, nil
}
