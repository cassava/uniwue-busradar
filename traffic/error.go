// Copyright (c) 2014, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package traffic

import "fmt"

type ParseError struct {
	File    string
	Line    int
	Message string
}

func NewParseError(file string, v interface{}) *ParseError {
	msg := fmt.Sprintf("%v", v)
	return &ParseError{
		File:    file,
		Line:    -1,
		Message: msg,
	}
}

func (pe *ParseError) Error() string {
	if pe.Line >= 0 {
		return fmt.Sprintf("%s:%s: %s", pe.File, pe.Line, pe.Message)
	}
	return fmt.Sprintf("%s: %s", pe.File, pe.Message)
}
