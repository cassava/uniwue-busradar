// Copyright (c) 2014, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package traffic

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"time"
)

// Route represents a route that a bus travels along. This is what the GPS
// sender would possibly send. It is simply a list of coordinates and an ID.
type Route struct {
	ID          string
	Line        *Line
	Path        []Coord
	StopIndexes []int
}

func (r *Route) WaitTime(roundtrip time.Duration) time.Duration {
	n := len(r.Path)
	if n > 0 {
		return time.Duration(float64(roundtrip) / float64(n))
	}
	return 0
}

func (r *Route) valid() error {
	if len(r.Path) == 0 {
		return errors.New("path is empty")
	} else if len(r.StopIndexes) == 0 {
		return errors.New("stop indexes array is empty")
	}
	return nil
}

// ReadRoute loads a route from file. The route should consist
// only of lines with a single coordinate, such as:
//
//	49.56782,9.34561
//	49.56785,9.34565
//	...
//
func ReadRoute(filename string, m map[string]*Line) (*Route, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	buf := bufio.NewReader(file)

	opt, err := readOptions(buf)
	if err != nil {
		return nil, NewParseError(filename, err)
	}

	// Get options from the file.
	var r Route
	r.ID = getOpt(opt, "route", "")
	if len(r.ID) == 0 {
		return nil, NewParseError(filename, "option route is required")
	}
	line := getOpt(opt, "line", "")
	if len(line) == 0 {
		return nil, NewParseError(filename, "option line is required")
	}
	var ok bool
	r.Line, ok = m[line]
	if !ok {
		return nil, NewParseError(filename, fmt.Sprintf("line %q unavailable", line))
	}

	// Read the route from the file.
	err = foreachLine(buf, func(s string) error {
		c, err := ParseCoord(s)
		if err != nil {
			return err
		}
		r.Path = append(r.Path, c)
		return nil
	})
	if err != nil {
		return nil, NewParseError(filename, err)
	}

	r.StopIndexes = findStopIndexes(r.Line, r.Path)

	return &r, nil
}

// ReadAllRoutes reads all the routes from the given files.
//
// Errors are handled in the same way as ReadAllStops.
func ReadAllRoutes(filenames []string, m map[string]*Line, ch chan<- error) []*Route {
	list := make([]*Route, 0, len(filenames))
	for _, f := range filenames {
		r, err := ReadRoute(f, m)
		if err != nil {
			if ch != nil {
				ch <- err
			}
			continue
		}

		list = append(list, r)
	}

	return list
}

// RoutesAsMap returns a map created from the input list of routes.
//
// Error handling is handled as in ReadAllLines.
// The errors that are returned should be treated more as warnings however.
func RoutesAsMap(routes []*Route, ch chan<- error) map[string]*Route {
	m := make(map[string]*Route)
	for _, r := range routes {
		if old, ok := m[r.ID]; ok {
			if ch != nil {
				ch <- fmt.Errorf("replacing line %q with %q", old, r)
			}
		}
		m[r.ID] = r
	}
	return m
}

// findStopIndexes calculates the indexes in the route that shall be marked as
// stops, based on the stops in the line.
func findStopIndexes(line *Line, cs []Coord) []int {
	n := len(cs)

	closest := make([]float64, len(line.Stops))
	for i, s := range line.Stops {
		min := s.Coord.Dist(cs[0])
		for i := 1; i < n; i++ {
			if d := s.Coord.Dist(cs[i]); d < min {
				min = d
			}
		}
		closest[i] = min
	}
	eps := maxFloat64(closest)

	stopPositions := make([]int, len(line.Stops))
outer:
	for {
		next := 0
		nextCoord := line.Stops[next].Coord
		for i := 0; i < n; i++ {
			if nextCoord.Dist(cs[i]) <= eps {
				stopPositions[next] = i
				next++
				if next == len(line.Stops) {
					break outer
				}
				nextCoord = line.Stops[next].Coord
			}
		}

		eps += eps * 0.05
	}

	return stopPositions
}

// maxFloat64 returns the maximum of the floats in the given array.
func maxFloat64(xs []float64) float64 {
	max := xs[0]
	for _, x := range xs {
		if x > max {
			max = x
		}
	}
	return max
}
