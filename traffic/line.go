// Copyright (c) 2014, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package traffic

import (
	"bufio"
	"fmt"
	"os"
)

type Line struct {
	ID    string
	Name  string
	Stops []*Stop
}

func ReadLine(filename string, m map[string]*Stop) (*Line, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	buf := bufio.NewReader(file)

	opt, err := readOptions(buf)
	if err != nil {
		return nil, NewParseError(filename, err)
	}

	var line Line
	line.ID = getOpt(opt, "line", "")
	if len(line.ID) == 0 {
		return nil, NewParseError(filename, "line option required")
	}
	line.Name = getOpt(opt, "name", line.ID)

	err = foreachLine(buf, func(k string) error {
		s, ok := m[k]
		if !ok {
			return NewParseError(filename, fmt.Sprintf("stop %q unavailable", k))
		}
		line.Stops = append(line.Stops, s)
		return nil
	})
	if err != nil {
		return nil, err
	}

	return &line, nil
}

// ReadAllLines reads all the lines from the given files.
//
// Errors are handled in the same way as ReadAllStops.
func ReadAllLines(filenames []string, m map[string]*Stop, ch chan<- error) []*Line {
	list := make([]*Line, 0, len(filenames))
	for _, f := range filenames {
		l, err := ReadLine(f, m)
		if err != nil {
			if ch != nil {
				ch <- err
			}
			continue
		}

		list = append(list, l)
	}

	return list
}

// LinesAsMap returns a map created from the input list of lines.
//
// Error handling is handled as in ReadAllLines.
// The errors that are returned should be treated more as warnings however.
func LinesAsMap(lines []*Line, ch chan<- error) map[string]*Line {
	lm := make(map[string]*Line)
	for _, l := range lines {
		if old, ok := lm[l.ID]; ok {
			if ch != nil {
				ch <- fmt.Errorf("replacing line %q with %q", old, l)
			}
		}
		lm[l.ID] = l
	}
	return lm
}
