// Copyright (c) 2014, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package traffic

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type Stop struct {
	ID        string
	Name      string
	Direction `json:"-"`
	Coord     Coord
}

func NewStop(id string, c Coord) *Stop {
	name, direction := parseStopID(id)
	return &Stop{
		ID:        id,
		Name:      name,
		Direction: direction,
		Coord:     c,
	}
}

func parseStopID(s string) (name string, dir Direction) {
	i := strings.LastIndex(s, " ")
	d := ParseDirection(s[i+1:])
	return s[:i], d
}

func parseStop(s string) (*Stop, error) {
	arr := strings.Split(s, ",")
	if len(arr) != 3 {
		return nil, fmt.Errorf("cannot parse stop %q", s)
	}

	c, err := parseCoordParts(arr[0], arr[1])
	if err != nil {
		return nil, err
	}

	return NewStop(arr[2], c), nil
}

func (s *Stop) String() string {
	return fmt.Sprintf("%s:%s", s.ID, s.Coord)
}

func ReadStops(filename string) ([]*Stop, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, NewParseError(filename, err)
	}
	defer file.Close()
	buf := bufio.NewReader(file)

	var stops []*Stop
	err = foreachLine(buf, func(line string) error {
		s, err := parseStop(line)
		if err != nil {
			return err
		}
		stops = append(stops, s)
		return nil
	})
	if err != nil {
		return nil, NewParseError(filename, err)
	}

	return stops, nil
}

// ReadAllStops reads all the stops from the given files.
//
// Errors are passed through the channel if the channel is not nil, otherwise
// they are ignored. Make sure you handle the errors right away, like so:
//
//	ch := make(chan error)
//	go func() {
//		for err := range ch {
//			fmt.Println("error:", err)
//		}
//	}()
//	pkgs := ReadAllStops(files, ch)
//	close(ch)
//
// Because if you don't, the program will probably run into a deadlock when
// there is an error. Note that ReadAllStops does not close the channel, you
// have to do that yourself.
func ReadAllStops(filenames []string, ch chan<- error) []*Stop {
	var n int
	sl := make([][]*Stop, 0, len(filenames))
	for _, f := range filenames {
		s, err := ReadStops(f)
		if err != nil {
			if ch != nil {
				ch <- err
			}
			continue
		}

		sl = append(sl, s)
		n += len(s)
	}

	i := 0
	stops := make([]*Stop, n)
	for _, ss := range sl {
		for _, s := range ss {
			stops[i] = s
			i++
		}
	}
	return stops
}

// StopsAsMap returns a map created from the input list of stops.
//
// Error handling is handled as in ReadAllStops.
// The errors that are returned should be treated more as warnings however.
func StopsAsMap(stops []*Stop, ch chan<- error) map[string]*Stop {
	sm := make(map[string]*Stop)
	for _, s := range stops {
		if old, ok := sm[s.ID]; ok {
			if ch != nil {
				ch <- fmt.Errorf("replacing stop %q with %q", old, s)
			}
		}
		sm[s.ID] = s
	}
	return sm
}

func SimplifyStops(stops []*Stop) []*Stop {
	sm := make(map[string]*Stop)
	for _, s := range stops {
		var nc Coord
		if old, ok := sm[s.Name]; ok {
			nc = s.Coord.Midpoint(old.Coord)
		} else {
			nc = s.Coord
		}
		sm[s.Name] = &Stop{
			ID:        s.Name,
			Name:      s.Name,
			Direction: UnknownDirection,
			Coord:     nc,
		}
	}

	out := make([]*Stop, 0, len(sm))
	for _, v := range sm {
		out = append(out, v)
	}
	return out
}
