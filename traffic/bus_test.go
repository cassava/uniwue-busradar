// Copyright (c) 2014, Ben Morgan. All rights reserved.
// Use of this source code is governed by an MIT license
// that can be found in the LICENSE file.

package traffic

import (
	"encoding/json"
	"regexp"
	"testing"
	"time"
)

// TestBusAnomaly doesn't really test anything, but it tries to find
// an error. It hasn't worked so far, so it's not a reliable test.
func TestBusAnomaly(t *testing.T) {
	const freq = time.Second / 10
	var dur = 5 * time.Minute
	if testing.Short() {
		t.Skip("skipping test in short mode")
	}

	ch := make(chan error)
	go func() {
		for err := range ch {
			if err != nil {
				t.Error(err)
			}
		}
	}()

	tra, err := ReadDir("testdata", ch)
	if err != nil {
		t.Fatal(err)
	}

	for _, b := range tra.Buses {
		b.Start()
	}
	defer func() {
		for _, b := range tra.Buses {
			b.Reset()
		}
	}()

	r := regexp.MustCompile(`\d+E\d+`)
	end := time.Now().Add(dur)
	for end.After(time.Now()) {
		bs, err := json.Marshal(tra.Buses)
		if err != nil {
			t.Fatalf("error marshaling buses: %s", err)
		}
		if r.Match(bs) {
			t.Errorf("fails at: %s", string(bs))
		}
	}
}
